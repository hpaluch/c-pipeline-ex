# Example C/C++ pipeline for GitLab

Here is simple parametrized GitLab Pipeline to build both `x86_64` 64-bit  and `x86` 32-bit binaries.
It uses out-of-tree builds (with help of VPATH variable on GNU Make) to avoid races. Please
see https://www.gnu.org/software/make/manual/html_node/General-Search.html for more details
on `VPATH` feature of GNU Make.


# Setup

Tested on:
- OS: `openSUSE Leap 15.1` on `x86_64` platform
- `GitLab CE 13.0.6` rpm installation - tested `gitlab-ce-13.0.6-ce.0.sles15.x86_64.rpm`
- `gitlab-runner` registered to GitLab and configured with `shell executor` - must
  have tag `shared1` to match this pipeline tag.

> NOTE: GitLab and `gitlab-runner` setup is out of scope of this readme.

You need to install following packages on `gitlab-runner` on openSUSE to build C/C++ programs both 32-bit and 64-bit:

```bash
sudo zypper in git-core make gcc gcc-c++ gcc-32bit gcc-c++-32bit glibc-devel-32bit
```

Then create new project in your GitLab and push this project to your GitLab. It should run
Pipeline in this project...

To test basic syntax, try:

```bash
yamllint .gitlab-ci.yml
```

To run any of these two jobs locally you can try:

```bash
# for x86_64
gitlab-runner exec shell build_x86_64_job
# for i686 cross-build
gitlab-runner exec shell build_x86_job
```

# Resources

* Template is based on https://stackoverflow.com/a/41199878

